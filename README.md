# N'oeuf en Chocolat

#### 19/03/2016 — Sujet : *Lapin*

Principe de jeu
--
C'est un Sokoban classique : pousser tous les oeufs dans les nids pour finir les niveaux. On ne peut pas pousser deux oeufs à la fois, et on ne peut pas non plus les tirer. Featuring un lapin trop mignon !

Commandes
--
Flèches : se déplacer et pousser les oeufs  
Backspace : annuler des mouvements  
R : recommencer le niveau  

Dépendances
--
    glk (git@gitlab.com:D-z/glk.git)
     + Boost 1.59
     + GLM 0.9.7.1
     + Un compilo qui bugue pas trop (Clang 3.6.0 est OK)
    SDL2
    SDL2_image
    OpenGL
    GLM 0.9.7.1
    GLEW
    GLU

La version adéquate de GLK est le tag "lapeing" (:D) qui est checkout dans le submodule /glk.
