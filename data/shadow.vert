#version 330

uniform mat3 pvMatrix;

layout(location = 0) in vec2 vertexCoord;
layout(location = 1) in mat3x2 modelMatrix;

out VSOut {
	vec2 uv;
} _out;

void main() {
	gl_Position = vec4(pvMatrix * mat3(modelMatrix) * vec3(vertexCoord, 1.0f), 1.0);
	_out.uv = vertexCoord;
}
