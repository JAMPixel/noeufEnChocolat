#version 330

uniform sampler2D spritesheet;

in VSOut {
	vec2 uv;
	vec4 color;
	vec4 tint;
} _in;

out vec4 col;

void main() {
	vec4 c = texelFetch(spritesheet, ivec2(_in.uv), 0) * _in.color;
	col = mix(c, vec4(_in.tint.rgb, c.a), _in.tint.a);
}
