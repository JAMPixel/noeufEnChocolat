#version 330

uniform mat3 pvMatrix;

layout(location = 0) in vec2 vertexCoord;
layout(location = 1) in mat3x2 modelMatrix;
layout(location = 4) in ivec2 tlUv;
layout(location = 5) in ivec2 brUv;
layout(location = 6) in vec4 color;
layout(location = 7) in vec4 tint;

out VSOut {
	vec2 uv;
	vec4 color;
	vec4 tint;
} _out;

void main() {
	gl_Position = vec4(pvMatrix * mat3(modelMatrix) * vec3(vertexCoord, 1.0f), 1.0);
	_out.uv = mix(tlUv, brUv, vertexCoord);
	_out.color = color;
	_out.tint = tint;
}
