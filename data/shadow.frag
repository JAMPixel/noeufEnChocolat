#version 330

in VSOut {
	vec2 uv;
} _in;

out vec4 col;

void main() {
	vec2 c = _in.uv - 0.5;
	float d = dot(c, c);
	col = vec4(0.0, 0.0, 0.0, 0.3 * smoothstep(0.2, 0.0, d));
}
