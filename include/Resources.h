#pragma once

#include <glk/gl/Texture.h>

#include "SpriteStrip.h"

struct Resources {
	Resources();

	glk::gl::TextureName tex;

	struct {
		struct {
			SpriteStrip left, right, up, down;
		} bunny;

		SpriteStrip noeufs, tiles;
	} strip;
};
