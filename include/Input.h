#pragma once

#include <array>
#include <cstdint>

struct Input {
	Input();

	void update();

	bool goLeft() const;
	bool goRight() const;
	bool goUp() const;
	bool goDown() const;

	bool undo() const;
	bool restart() const;

private:
	std::uint8_t const *_keys;
	std::array<bool, 6> _prev;
};
