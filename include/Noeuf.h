#pragma once

#include <glk/DepInject.h>

#include "MovingObject.h"
#include "VisibleObject.h"

struct Noeuf : MovingObject, VisibleObject {
	Noeuf(glk::DIContainer &dic, glm::ivec2 pos);

	void update();

	float deform = -1.0f;
};
