#pragma once

#include <glm/vec2.hpp>

struct SpriteStrip {

	glm::ivec2 framePos(int n) const {
		n = ((n % frames) + frames) % frames;
		int r = n / columns, c = n % columns;
		return topLeft + (frameSize + padding) * glm::ivec2{c, r};
	}

	glm::ivec2 frameSize;
	glm::ivec2 topLeft{0};
	int frames = 1;
	int columns = frames;
	glm::ivec2 padding{0};
};
