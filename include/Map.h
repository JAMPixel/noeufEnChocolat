#pragma once

#include <boost/multi_array.hpp>

#include <glm/vec2.hpp>

#include <glk/DepInject.h>

struct Noeuf;
struct Coords;

enum class Tile : unsigned char {
	rien,
	mur,
	nid
};

struct Map {
	Map(glk::DIContainer &dic, std::string const &fName);
	~Map();

	bool solid(glm::ivec2 pos);

	glm::ivec2 size, startCoords;
	boost::multi_array<Tile, 2u> tiles;
	std::vector<Noeuf> noeufs;
};
