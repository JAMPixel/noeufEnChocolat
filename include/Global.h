#pragma once

#include <memory>
#include <array>
#include <vector>
#include <stack>

#include <glk/InPlace.h>
#include <glk/DepInject.h>
#include <glk/gl/Vbo.h>
#include <glk/gl/VertexAttribs.h>
#include <glk/gl/InstanceQueue.h>
#include <glk/gl/Texture.h>

#include "Resources.h"
#include "Input.h"
#include "Programs.h"
#include "SpriteAttribs.h"
#include "Map.h"

struct Bunny;
struct Noeuf;
struct Map;
struct UndoMove;

struct Global {
	static int const W_WIDTH, W_HEIGHT;
	static float const PX_PER_METER, METERS_PER_PX;
	static float const M_WIDTH, M_HEIGHT;

	using SpriteQueue = glk::gl::InstanceQueue<glm::vec2, SpriteAttribs>;

	Global();
	~Global();

	void update();
	void display();

	bool mapFree(glm::ivec2 pos, Noeuf *&noeuf);
	bool mapFree(glm::ivec2 pos) {
		Noeuf *n = nullptr;
		return mapFree(pos, n) && !n;
	}

	std::vector<Noeuf> &noeufs() { return map->noeufs; }
	std::vector<Noeuf> const &noeufs() const { return map->noeufs; }

	bool levelFinished() const;

	void loadLevel(std::string const &fName);

	Resources res;
	Input input;
	glk::gl::Vbo<glm::vec2> unitVbo;
	glk::DIContainer dic;
	Programs prog;

	struct {
		SpriteQueue map, objects;
		glk::gl::InstanceQueue<glm::vec2, glm::mat3x2> shadows;
	} dq;

	std::unique_ptr<Map> map;
	std::unique_ptr<Bunny> bunny;

	std::stack<UndoMove> undoStack;

	glm::mat3 const cam;

	std::vector<std::string> lesLevels;
	int levelCourant;

	bool nextLevel();

	void restart();

	void endGame();

};

extern glk::InPlace<Global> global;
