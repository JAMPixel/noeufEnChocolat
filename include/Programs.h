#pragma once

#include <glk/DepInject.h>
#include <glk/gl/Program.h>

struct Programs {

	Programs(glk::DIContainer &dic);

	GLK_GL_PROGRAM(Sprite,
		(glm::mat3, pvMatrix)
		(glk::gl::Sampler2d, spritesheet)
	) sprite{
		glk::gl::VertShader{"data/sprite.vert"},
		glk::gl::FragShader{"data/sprite.frag"}
	};

	GLK_GL_PROGRAM(Shadow,
		(glm::mat3, pvMatrix)
	) shadow{
		glk::gl::VertShader{"data/shadow.vert"},
		glk::gl::FragShader{"data/shadow.frag"}
	};
};
