#pragma once

#include <functional>

#include <glm/vec2.hpp>

struct Coords {

	constexpr Coords()
		: Coords(0, 0) { }

	constexpr Coords(int x, int y)
		: x(x), y(y) { }

	constexpr bool operator ==(Coords const &rhs) const {
		return x == rhs.x && y == rhs.y;
	}

	constexpr bool operator !=(Coords const &rhs) const {
		return !(*this == rhs);
	}

	Coords &operator +=(Coords const &rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	Coords operator +(Coords const &rhs) const {
		return Coords(*this) += rhs;
	}

	Coords &operator -=(Coords const &rhs) {
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	Coords operator -(Coords const &rhs) const {
		return Coords(*this) -= rhs;
	}

	constexpr Coords operator /(int i) const {
		return Coords(x / i, y / i);
	}

	auto vec() const {
		return glm::vec2(x, y);
	}

	auto ivec() const {
		return glm::ivec2(x, y);
	}

	int x, y;
};

struct OffsetCoords
	: Coords {
	using Coords::Coords;

	OffsetCoords() { }

	OffsetCoords(Coords const &coords)
		: Coords(coords) { }

	bool operator ==(OffsetCoords const &rhs) const {
		return sameTile(rhs) && offset == rhs.offset;
	}

	bool sameTile(OffsetCoords const &other) const {
		return Coords::operator ==(other);
	}

	OffsetCoords operator -(OffsetCoords const &rhs) const {
		return OffsetCoords(x - rhs.x, y - rhs.y);
	}

	auto vec() const {
		return glm::vec2(x, y) + offset;
	}

	glm::vec2 offset;
};

namespace std {

	template <>
	struct hash<Coords> {
		static_assert(sizeof(int) == sizeof(long long) / 2, "Wrong types sizes, please patch std::hash<Coords>.");

		std::size_t operator ()(Coords coords) const {
			return std::hash<long long>()(
				static_cast<long long> (coords.x) << sizeof(long long) / 2
					| coords.y
			);
		}
	};
}
