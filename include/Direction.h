#pragma once

enum class Dir {
	LEFT, RIGHT, UP, DOWN
};
