#pragma once

#include <glm/vec2.hpp>
#include <glk/DepInject.h>

#include "Coords.h"
#include "Sprite.h"
#include "MovingObject.h"
#include "VisibleObject.h"
#include "Noeuf.h"

struct Bunny : MovingObject, VisibleObject {
	Bunny(glk::DIContainer &dic, glm::ivec2 pos);

	void update();
	bool tryMoveAndPush(glm::ivec2 dest);

	float deform = -1.0f;
};
