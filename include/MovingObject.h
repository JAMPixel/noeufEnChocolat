#pragma once

#include <glm/vec2.hpp>
#include <glm/mat3x2.hpp>

#include "Coords.h"

struct MovingObject {
	bool update();

	void moveTo(glm::ivec2 to);

	glm::mat3x2 makeShadowAttribs() const;

	OffsetCoords pos;
};
