#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include "SpriteStrip.h"

struct SpriteAttribs;

struct Sprite {

	SpriteStrip const *strip;
	glm::vec2 position;
	float frame = 0.0f;

	glm::vec2 scale{1.0f};
	glm::vec2 origin{0.0f};
	float angle = 0.0f;

	glm::vec4 color{1.0f};
	glm::vec4 tint{0.0f};

	SpriteAttribs makeAttribs() const;
};
