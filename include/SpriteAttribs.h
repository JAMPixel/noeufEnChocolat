#pragma once

#include <glk/gl/VertexAttribs.h>

//fmtoff
GLK_GL_ATTRIB_STRUCT(
	SpriteAttribs,
	(glm::mat3x2, transform)
	(glm::ivec2, tlUv)
	(glm::ivec2, brUv)
	(glm::vec4, color)
	(glm::vec4, tint)
);
//fmton
