#include "Bunny.h"

#include <iostream>

#include <glk/util.h>

#include "Input.h"
#include "Global.h"
#include "Direction.h"
#include "Resources.h"
#include "Map.h"
#include "UndoMove.h"

Bunny::Bunny(glk::DIContainer &dic, glm::ivec2 pos)
: MovingObject{{pos.x, pos.y}}
, VisibleObject{{&dic.get<Resources>().strip.bunny.right}} {
	sprite.scale = glm::vec2{1.0f / 200.0f};
	sprite.origin = glm::vec2{100.0f};
}

void Bunny::update() {
	Input &input = global->input;
	Resources &res = global->res;

	if(MovingObject::update())
		deform = 0.2f;

	deform *= 0.75f;

	int dx = 0, dy = 0;

	if (input.goLeft()) {
		--dx;
		sprite.strip = &res.strip.bunny.left;
	} else if (input.goRight()) {
		++dx;
		sprite.strip = &res.strip.bunny.right;
	} else if (input.goUp()) {
		--dy;
		sprite.strip = &res.strip.bunny.up;
	} else if (input.goDown()) {
		++dy;
		sprite.strip = &res.strip.bunny.down;
	} else if (input.undo() && !global->undoStack.empty()) {
		UndoMove um = global->undoStack.top();
		global->undoStack.pop();
		if (um.noeuf)
			um.noeuf->moveTo(pos.ivec());
		moveTo(um.bunnyPos);
	}

	if (dx || dy) {
		glm::ivec2 dest = pos.ivec() + glm::ivec2{dx, dy};
		if (tryMoveAndPush(dest))
			MovingObject::moveTo(dest);
	}

	sprite.scale = glm::vec2{
		(1.0f + deform) / 200.0f,
		(1.0f - deform) / 200.0f
	};
	sprite.position = pos.vec() + glm::vec2{0.0f, -0.25f} + glm::vec2{0.5f, 0.5f + 0.5f * deform};
	float d = glm::length(pos.offset);

	if(d) {
		sprite.position.y += d * (d - 1.0f);
		sprite.angle = -30.0f * (d - 0.5f) * glk::sign(pos.offset.x);
	}
	else
		sprite.angle = 0.0f;
}

bool Bunny::tryMoveAndPush(glm::ivec2 dest) {

	UndoMove um{pos.ivec()};

	if (!global->mapFree(dest, um.noeuf))
		return false;

	if (!um.noeuf) {
		global->undoStack.push(um);
		return true;
	}

	glm::ivec2 into = 2 * um.noeuf->pos.ivec() - pos.ivec();

	if (!global->mapFree(into))
		return false;

	um.noeuf->moveTo(into);

	global->undoStack.push(um);
	return true;
}
