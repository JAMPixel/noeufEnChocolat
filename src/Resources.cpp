#include "Resources.h"

namespace {
	constexpr float FRAME_W = 200.0f, FRAME_H = 200.0f;
	glm::vec2 const FRAME_SIZE{FRAME_W, FRAME_H};
}

//fmtoff
Resources::Resources()
: tex {glk::gl::loadTextureFromFile("data/sprite/theSprite.png")}
, strip {
	{
		{FRAME_SIZE, {0 * FRAME_W, 0.0f}},
		{FRAME_SIZE, {1 * FRAME_W, 0.0f}},
		{FRAME_SIZE, {2 * FRAME_W, 0.0f}},
		{FRAME_SIZE, {3 * FRAME_W, 0.0f}},
	},
    {FRAME_SIZE, {0 * FRAME_W, 2 * FRAME_H}},
    {FRAME_SIZE, {1 * FRAME_W, 2 * FRAME_H}, 2}
} { }
//fmton
