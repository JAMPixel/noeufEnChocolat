#include "Global.h"

#include <memory>
#include <random>
#include <ctime>
#include <algorithm>
#include <iostream>

#include <glm/glm.hpp>
#include <glk/InPlace.h>
#include <Global.h>

#include "Resources.h"
#include "Bunny.h"
#include "Noeuf.h"
#include "Map.h"
#include "UndoMove.h"

glk::InPlace<Global> global;

int const Global::W_WIDTH = 1024, Global::W_HEIGHT = 640;
float const Global::PX_PER_METER = 64.0f, Global::METERS_PER_PX = 1.0f / Global::PX_PER_METER;
float const Global::M_WIDTH = Global::W_WIDTH * Global::METERS_PER_PX, Global::M_HEIGHT =
	Global::W_HEIGHT * Global::METERS_PER_PX;

namespace {
	std::array<glm::vec2, 4> const unitQuad{{
		                                        {0.0f, 0.0f},
		                                        {1.0f, 0.0f},
		                                        {0.0f, 1.0f},
		                                        {1.0f, 1.0f}
	                                        }};
}

Global::Global()
	: unitVbo{begin(unitQuad), end(unitQuad)}
	  , dic{unitVbo}
	  , prog{dic}
	  , dq{
		{unitVbo},
		{unitVbo},
		{unitVbo}
	}
	  , cam{
		2.0f / M_WIDTH, 0.0f, 0.0f,
		0.0f, -2.0f / M_HEIGHT, 0.0f,
		-1.0f, 1.0f, 1.0f
	} {
	dic.insert(res);

	prog.sprite.use();
	prog.sprite.pvMatrix = cam;

	prog.shadow.use();
	prog.shadow.pvMatrix = cam;

	lesLevels.push_back("data/level0");
	lesLevels.push_back("data/level1");
	levelCourant = 0;

	loadLevel(lesLevels[levelCourant]);
}

Global::~Global() = default;


void Global::update() {
	bunny->update();
	for (Noeuf &n : noeufs())
		n.update();
}

void Global::display() {
	TRY_GL(glActiveTexture(GL_TEXTURE0));
	TRY_GL(glBindTexture(GL_TEXTURE_2D, res.tex));

	for (Noeuf const &n : noeufs()) {
		dq.shadows.enqueue(n.makeShadowAttribs());
		dq.objects.enqueue(n.sprite.makeAttribs());
	}

	{
		dq.shadows.enqueue(bunny->makeShadowAttribs());
		dq.objects.enqueue(bunny->sprite.makeAttribs());
	}
	{
		prog.sprite.use();
		prog.sprite.spritesheet = 0;

		dq.map.display();

		prog.shadow.use();
		dq.shadows.displayAndClear();

		prog.sprite.use();
		dq.objects.displayAndClear();
	}
}

bool Global::mapFree(glm::ivec2 pos, Noeuf *&noeuf) {
	if (map->solid(pos))
		return false;

	auto n = std::find_if(begin(noeufs()), end(noeufs()), [pos](Noeuf const &n) {
		return n.pos.ivec() == pos;
	});

	if (n != end(noeufs()))
		noeuf = &*n;

	return true;
}

bool Global::levelFinished() const {
	return std::find_if(begin(noeufs()), end(noeufs()), [&m = *map](Noeuf const &n) {
		return m.tiles[n.pos.x][n.pos.y] != Tile::nid;
	}) == end(noeufs());
}


void Global::loadLevel(std::string const &fName) {
	decltype(undoStack){}.swap(undoStack);
	map = std::make_unique<Map>(dic, fName);
	bunny = std::make_unique<Bunny>(dic, map->startCoords);

	dq.map.clear();

	Sprite tile{&res.strip.tiles};
	tile.scale = glm::vec2{1.0f / 200.0f};
	for (std::size_t y = 0u; y < map->tiles.shape()[1]; ++y)
		for (std::size_t x = 0u; x < map->tiles.shape()[0]; ++x) {
			tile.position = glm::vec2{x, y};
			switch (map->tiles[x][y]) {
				case Tile::mur:
					tile.frame = 1;
					dq.map.enqueue(tile.makeAttribs());
					break;
				case Tile::nid:
					tile.frame = 0;
					dq.map.enqueue(tile.makeAttribs());
					break;
			}
		}
}

bool Global::nextLevel(){
	levelCourant ++;
	if(levelCourant < lesLevels.size()){
		loadLevel(lesLevels[levelCourant]);
		return true;
	}
	return false;
}

void Global::restart(){
	std::cout << "RESTART" << std::endl;
	loadLevel(lesLevels[levelCourant]);
}

void Global::endGame() {
	std::cout << "Vous avez gagné !" << std::endl;
}