#include "Input.h"

#include <SDL2/SDL_keyboard.h>

#include <glk/util.h>

#include "Direction.h"

namespace {
	constexpr auto K_LEFT = SDL_SCANCODE_LEFT;
	constexpr auto K_RIGHT = SDL_SCANCODE_RIGHT;
	constexpr auto K_UP = SDL_SCANCODE_UP;
	constexpr auto K_DOWN = SDL_SCANCODE_DOWN;
	constexpr auto K_UNDO = SDL_SCANCODE_BACKSPACE;
	constexpr auto K_RESTART = SDL_SCANCODE_R;
}

Input::Input()
	: _keys{SDL_GetKeyboardState(nullptr)} { }

void Input::update() {
	_prev[glk::val(Dir::LEFT)] = _keys[K_LEFT];
	_prev[glk::val(Dir::RIGHT)] = _keys[K_RIGHT];
	_prev[glk::val(Dir::UP)] = _keys[K_UP];
	_prev[glk::val(Dir::DOWN)] = _keys[K_DOWN];
	_prev[5] = _keys[K_UNDO];
	_prev[6] = _keys[K_RESTART];
}

bool Input::goLeft() const {
	return _keys[K_LEFT] && !_prev[glk::val(Dir::LEFT)];
}

bool Input::goRight() const {
	return _keys[K_RIGHT] && !_prev[glk::val(Dir::RIGHT)];
}

bool Input::goUp() const {
	return _keys[K_UP] && !_prev[glk::val(Dir::UP)];
}

bool Input::goDown() const {
	return _keys[K_DOWN] && !_prev[glk::val(Dir::DOWN)];
}

bool Input::undo() const {
	return _keys[K_UNDO] && !_prev[5];
}

bool Input::restart() const {
	return _keys[K_RESTART] && !_prev[6];
}