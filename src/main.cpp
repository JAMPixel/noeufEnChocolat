#include <iostream>

#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_events.h>

#include <GL/glew.h>

#include <glk/core.h>
#include <glk/error.h>
#include <glk/gl/util.h>
#include <SpriteAttribs.h>
#include <Sprite.h>

#include "Global.h"

int main(int, char **) {

	std::ios::sync_with_stdio(false);

	auto sdlWindow = SDL_CreateWindow(
		"Noeuf en Chocolat",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		Global::W_WIDTH,
		Global::W_HEIGHT,
		SDL_WINDOW_OPENGL
	);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	auto glContext = SDL_GL_CreateContext(sdlWindow);
	assert(glContext);
	SDL_GL_MakeCurrent(sdlWindow, glContext);

	SDL_ShowWindow(sdlWindow);
	SDL_ShowCursor(false);

	glewExperimental = true;
	GLenum glewStatus =glewInit();
	if(glewStatus != GLEW_OK) {
		std::cerr << "Could not initialize GLEW, error : " << glewGetErrorString(glewStatus) << '\n';
		return 1;
	}

	(void)glGetError();

	{
		int maj, min;
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &maj);
		SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &min);
		std::cout << "Initialized OpenGL " << maj << "." << min << std::endl;
	}

	// For 1 byte-per-pixel textures
	TRY_GL(glPixelStorei(GL_PACK_ALIGNMENT, 1));
	TRY_GL(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	TRY_GL(glClearColor(0.0f, 0.6f, 0.2f, 1.0f));

	TRY_GL(glEnable(GL_BLEND));
	TRY_GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	GLint maxTextureUnits;
	TRY_GL(glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextureUnits));

	GLuint sampler;
	TRY_GL(glGenSamplers(1, &sampler));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	TRY_GL(glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST));

	for(int i = 0u; i < maxTextureUnits; ++i) {
		TRY_GL(glActiveTexture(GL_TEXTURE0 + i));
		TRY_GL(glBindSampler(i, sampler));
	}

	global.construct();
	int timer = 0;
	bool loop = true;
	do {
		global->input.update();

		SDL_Event ev;
		while(SDL_PollEvent(&ev)) {
			if(ev.type == SDL_QUIT
			   || (ev.type == SDL_KEYDOWN && ev.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
				loop = false;
		}

		global->update();

		TRY_GL(glClear(GL_COLOR_BUFFER_BIT));

		if(global->input.restart())
			global->restart();

		if(global->levelFinished()) {
			++timer;
			if(timer == 60) {
				timer = 0;
				if(!global->nextLevel()) {
					global->endGame();
					loop = false;
				}
			}
		}


		global->display();

		SDL_GL_SwapWindow(sdlWindow);

//		SDL_Delay(16u);
	} while(loop);

	global.destruct();

	TRY_GL(glDeleteSamplers(1, &sampler));

	return 0;
}