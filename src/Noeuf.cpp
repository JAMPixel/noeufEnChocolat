#include "Noeuf.h"

#include <ctime>
#include <cmath>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "Resources.h"

namespace {
	glm::vec3 hue(float h) {
		return glm::clamp(glm::vec3{
			std::abs(h * 6.0f - 3.0f) - 1.0f,
			2.0f - std::abs(h * 6.0f - 2.0f),
			2.0f - std::abs(h * 6.0f - 4.0f)
		}, glm::vec3{}, glm::vec3{1.0f});
	}

	glm::vec3 hsv2rgb(glm::vec3 hsv) {
		return {((hue(hsv.x) - 1.0f) * hsv.y + 1.0f) * hsv.z};
	}

	std::mt19937 prng(std::time(nullptr));
}

Noeuf::Noeuf(glk::DIContainer &dic, glm::ivec2 pos)
	: VisibleObject{{&dic.get<Resources>().strip.noeufs}} {
	MovingObject::pos.x = pos.x;
	MovingObject::pos.y = pos.y;

	sprite.scale = glm::vec2{1.0f / 200.0f};
	sprite.origin = glm::vec2{100.0f};

	sprite.color = glm::vec4{hsv2rgb({std::uniform_real_distribution<float>{0.0f, 1.0f}(prng), 0.5f, 1.0f}), 1.0f};
}

void Noeuf::update() {
	if(MovingObject::update())
		deform = 0.2f;

	deform *= 0.75f;

	float d = glm::length(pos.offset);
	sprite.scale = glm::vec2{
		(1.0f + deform) / 200.0f,
		(1.0f - deform) / 200.0f
	};
	sprite.position = pos.vec() + glm::vec2{0.0f, -0.25f} + glm::vec2{0.5f, 0.5f + 0.75f * (d * (d - 1.0f)) + 0.5f * deform};
}
