#include "Sprite.h"

#include <cmath>

#include <glm/vec2.hpp>
#include <glm/matrix.hpp>

#include <glk/gl/Texture.h>

#include "SpriteAttribs.h"

SpriteAttribs Sprite::makeAttribs() const {
	SpriteAttribs attribs;

	attribs.tlUv = strip->framePos(frame);
	attribs.brUv = attribs.tlUv + strip->frameSize;

	{
		auto &tm = attribs.transform;

		float a = glk::deg2rad(-angle);
		float s = std::sin(a);
		float c = std::cos(a);

		glm::vec2 sc = scale * glm::vec2{strip->frameSize};

		tm[0][0] = c * sc.x;
		tm[1][1] = c * sc.y;
		tm[0][1] = s * sc.x;
		tm[1][0] = -s * sc.y;

		glm::vec2 o = -origin;

		tm[2][0] = c * o.x * scale.x - s * o.y * scale.y + position.x;
		tm[2][1] = s * o.x * scale.x + c * o.y * scale.y + position.y;
	}

	attribs.color = color;
	attribs.tint = tint;

	return attribs;
}
