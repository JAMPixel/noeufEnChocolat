#include "MovingObject.h"

#include <glm/glm.hpp>

namespace {
	constexpr float SPEED = 0.1f;
}

bool MovingObject::update() {
	if(pos.offset.x || pos.offset.y) {
		float offs = glm::length(pos.offset);

		if(offs <= SPEED) {
			pos.offset = glm::vec2{};
			return true;
		}

		pos.offset = (offs - SPEED) * glm::normalize(pos.offset);
	}

	return false;
}

void MovingObject::moveTo(glm::ivec2 to) {
	glm::vec2 diff = pos.ivec() - to;
	pos.x = to.x;
	pos.y = to.y;
	pos.offset = diff;
}

glm::mat3x2 MovingObject::makeShadowAttribs() const {
	return {
		0.8f, 0.0f,
		0.0f, 0.2f,
		0.1f + pos.vec().x, 0.6f + pos.vec().y
	};
}
