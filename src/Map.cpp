#include "Map.h"

#include <fstream>

#include "Noeuf.h"
#include "Coords.h"

Map::Map(glk::DIContainer &dic, std::string const &fName) {
	std::ifstream str{fName};
	str.exceptions(std::ios::badbit | std::ios::failbit);

	str >> size.x >> size.y;

	tiles.resize(boost::extents[size.x][size.y]);
	for (std::size_t y = 0u; y < tiles.shape()[1]; ++y)
		for (std::size_t x = 0u; x < tiles.shape()[0]; ++x) {
			int i;
			str >> i;
			switch (i) {
				case 0:
					tiles[x][y] = Tile::rien;
					break;
				case 1:
					tiles[x][y] = Tile::mur;
					break;
				case 2:
					noeufs.emplace_back(dic, glm::ivec2{x, y});
					break;
				case 3:
					tiles[x][y] = Tile::nid;
					break;
				case 4:
					startCoords = glm::ivec2{x, y};
					break;
			}
		}
}

Map::~Map() = default;

bool Map::solid(glm::ivec2 pos) {
	return pos.x < 0 || pos.y < 0
	       || pos.x >= size.x || pos.y >= size.y
	       || tiles[pos.x][pos.y] == Tile::mur;
}
