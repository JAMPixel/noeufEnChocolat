#include "Programs.h"

#include <glk/DepInject.h>

Programs::Programs(glk::DIContainer &dic) {
	dic.insert(sprite);
	dic.insert(shadow);
}
